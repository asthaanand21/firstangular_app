import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class SignupService {

  constructor(private http: HttpClient) {
  }

  public getCategory() {
    return this.http.get('192.168.1.118:3000/category/get');
  }

}
