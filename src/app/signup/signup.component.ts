import { Component, OnInit } from '@angular/core';
import { SignupService } from '../signup.service';
interface myCategory {
  obj: Object
}
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})

export class SignupComponent implements OnInit {
  title = 'Sign Up!';
  record = {};

  constructor(private signupService : SignupService) { }

  onClickMe() {
    this.signupService.getCategory().subscribe(data =>{
      console.log('->>>>>>>>>>>>>>>>>>>>------------------->>',data);
    });
  }
  ngOnInit() {

  }
}
