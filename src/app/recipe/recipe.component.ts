import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})

export class RecipeComponent implements OnInit {
  showHome : boolean;
  showRegisteration : boolean;
  showLogin : boolean;
  constructor() {}

  ngOnInit() {
    this.showHome = false;
    this.showRegisteration = false;
    this.showLogin = false;

  }
  public onSelect(){
    this.showRegisteration = false;
    this.showLogin = false;
    this.showHome = !this.showHome;

  }
  public register() {
    this.showLogin = false;
    this.showHome = false;
    this.showRegisteration = !this.showRegisteration;

  }
  public  login() {
    this.showRegisteration = false;
    this.showHome = false;
    this.showLogin = !this.showLogin;

  }
}
