import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  template:'<div class="home">This is my home page!!!</div>'
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
